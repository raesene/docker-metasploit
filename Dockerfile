FROM alpine:3.4

RUN apk update && apk upgrade && apk --update add \
    ruby ruby-irb ruby-rake bash tzdata libpcap-dev \
    readline-dev openssl-dev git curl libxml2-dev \
    libxslt-dev ruby-io-console ruby-dev sqlite-dev \
    build-base postgresql-dev libffi-dev libgcrypt \
    ruby-bigdecimal ncurses

RUN echo 'gem: --no-rdoc --no-ri' > /etc/gemrc

RUN gem install bundler

#This is needed to get nokogiri to build
RUN bundle config build.nokogiri --use-system-libraries

RUN git clone --depth=1 https://github.com/rapid7/metasploit-framework.git && \
    cd metasploit-framework && \
    bundle install